## Q17
``` 
happiness <- read.csv("/Users/labuelgh/Documents/UTC/AC04/happiness_csv.csv")
summary(happiness)

#Nous faisons le choix de nous baser sur le score de happiness pour l'étude de ce jeu de données
#Calcul des coefficients de corrélation entre les variables et le score de happiness
eco_h <- cor(happiness$Happiness.Score, happiness$Economy..GDP.per.Capita.)
print(eco_h)
sante_h <- cor(happiness$Happiness.Score, happiness$Health..Life.Expectancy.)
print(sante_h)
liberte_h <- cor(happiness$Happiness.Score, happiness$Freedom)
print(liberte_h)
famille_h <- cor(happiness$Happiness.Score, happiness$Family)
print(famille_h)
confiance_h <- cor(happiness$Happiness.Score, happiness$Trust..Government.Corruption.)
print(confiance_h)
generosite_h <- cor(happiness$Happiness.Score, happiness$Generosity)
print(generosite_h)

#D'après les coefficients de corrélation, 3 des paramètres suivent la même tendance avec un coefficient élevé (Economy, Health et Family). 

#Graphiques de dispersion 
plot(happiness$Economy..GDP.per.Capita.,happiness$Happiness.Score)
plot(happiness$Health..Life.Expectancy.,happiness$Happiness.Score)
plot(happiness$Freedom,happiness$Happiness.Score)
plot(happiness$Family,happiness$Happiness.Score)
plot(happiness$Trust..Government.Corruption.,happiness$Happiness.Score)
plot(happiness$Generosity,happiness$Happiness.Score)

#Les nuages de points confirment nos observations initiales. Economy est le paramètre avec un coefficient le plus élevé et le nuage de points qui est pertinent pour une régression linéaire.

```

## Q18
``` 
Xeco <- happiness$Economy..GDP.per.Capita.
Yh <- happiness$Happiness.Score
plot(Xeco, Yh, main = "Droite des moindres carrés", col="blue")
estimah <- a_estim(Xeco, Yh)
print(estimah)
estimbh <- b_estim(Xeco, Yh)
print(estimbh)
abline(estimah, estimbh, col = "red")
lmh <- lm(Yh~Xeco)
abline(lmh, col = "green")
summary(lmh)
#Nous avons estimé les paramètres grâce aux fonctions créées lors des questions précedentes et nous avons vérifié l'exactitude de nos estimations en les comparant à la fonction fournie par R. Nous remarquons que les droites des moindres carrés se confondent ce qui confirme la validité de nos fonctions.
```


## Q19
``` 
confint(lmh)
#               2.5 %   97.5 %
#(Intercept) 3.236006 3.761613
#Xeco        1.937667 2.498787
#Il y a 95% de chance que notre ordonnée à l'origine (intercept), estimah, se situe dans l'intervalle [3.236006; 3.761613] et que le coefficient de la pente (Xeco), estimbh, se situe dans l'intervalle [1.937667; 2.498787]
```

## Q20
``` 
coeff <- cor(Yh,Xeco)^2
#Nous pouvons accéder à la valeur de R2 via la fonctoin cor. Nous avons une valeur de 0,6099 soit 60,99% ici. Or plus la valeur est proche de 1, meilleure est l'adaptation du modèle. Nous avons donc une bonne adéquation du modèle de régression linéaire aux données.
```

## Q21
``` 
verification <- cov(Xeco,Yh)^2/(sd(Xeco)^2*sd(Yh)^2)
#Nous obtenons une valeur d'environ 0,6099 qui est identique à la valeur calculée à la question précédente.
```

## Q22
``` 
p_value <- summary(lmh)$coefficients["Xeco", "Pr(>|t|)"]
if (p_value < 0.05) {
  cat("Rejeter H0: Le coefficient de la pente est significativement différent de 0.\n")
} else {
  cat("Ne pas rejeter H0: Aucune preuve significative que le coefficient de la pente est différent de 0.\n")
}
#p_value a pour valeur 1.050538e-33 ce qui nous indique que b est différent de 0. 
```