---
title: "Projet_TP_Regression_lin_Etu"
date: "2024-04-20"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Q0 - Membres du groupe

-   Abu El Ghait Laura : 25%
-   Bateman Julie : 25%
-   Levasseur Louis : 25%
-   Vasseur Agathe : 25%

## II - Données simulées
```  {r}         
a0<-round(runif(1,3,15),2)  
b0<-round(runif(1,1,5),2)  
s0<-round(runif(1,1,3),2)
```

## Q1a

``` {r}          
n<-400
x <- runif(n,0,5)
```

## Q1b

``` {r}          
y <- a0 + b0 * x + rnorm(n, 0, sqrt(s0))
```

## Q2

```{r}         
plot(x, y, main="Nuage de points et droite de régression linéaire")
abline(a=a0, b=b0, col="red")
```

## Q3

```  {r}         
b_estim <- function(x, y) {
  SxY<- sum((x - mean(x)) * (y - mean(y))) / length(x)
  s2x <- sum((x - mean(x))^2) / length(x)
  return (SxY / s2x)
}

a_estim <- function(x, y) {
  return (mean(y) - b_estim(x,y) * mean(x))
}

sigma_estim <- function(x,y) {
  sommeSigma <- sum((y - a_estim(x,y) - b_estim(x,y) * x)^2)
  return (sommeSigma / (length(x)-2))
}
```

## Q4
Estimation de a, b et sigma2
``` {r}           
estim_a <- a_estim(x, y)
print(estim_a)
estim_b <- b_estim(x, y)
print(estim_b)
estim_sigma2 <- sigma_estim(x, y)
print(estim_sigma2)
```

## Q5

``` {r}          
plot(x, y, main="Régression linéaire et droite des moindres carrés", xlab="x", ylab="y", col="black")
abline(a=a0, b=b0, col="red")  
abline(a=estim_a, b=estim_b, col="green")
legend("bottomright", legend=c("Droite de régression", "Droite des moindres carrés"), col=c("red", "green"), lty=c(1, 1), merge=TRUE, cex=0.8)
```

## Q6

``` {r}           
estim_epsilon <- y-(estim_a + estim_b*x)
somme_estim_epsilon <- sum(estim_epsilon)
print(somme_estim_epsilon)
```
La somme est très proche de 0 (e-13) donc on peut considèrer la propriété des résidus vérifiée.

## Q7

``` {r}          
moyenne_x <- mean(x)
moyenne_y <- mean(y)
appartient_a_droite_moindres_carres <- moyenne_y == estim_a + estim_b*moyenne_x
print(appartient_a_droite_moindres_carres)
```
Renvoie Vrai donc (moyenne_x, moyenne_y) appartient bien à la droite des moindres carrés.

## Q8

``` {r}          
donnees <- data.frame(x, y)
reg<-lm(y~x, data = donnees)
summary(reg)
```

| X | fit | lwr | upr |
| ----------- | ----------- | ----------- | ----------- |
| 1 | 5.717037 | 5.596314 | 5.837760 |
| 1.5 | 6.826150 | 6.610828 | 7.041473 |

Residuals:

| Min | 1Q | Median | 3Q | Max | 
| ----------- | ----------- | ----------- | ----------- | ----------- |
| -4.0558 | -1.0442 | -0.0020 | 0.8625 | 4.0709 |

Coefficients:

| | Estimate | Std. Error | t value | Pr(>abs(t)) |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| (Intercept) | 14.14566 | 0.14998 | 94.31 | <2e-16 |
| x | 2.83021 | 0.05018 | 56.40 | <2e-16 |

(Intercept)/Estimate donne une estimation de a selon le modèle reg, et x/Estimate donne celle de b, et en effet elles sont bels et bien proches de a0 et b0.
De plus, on voit bien que les résidus s approchent de 0.


## Q9

Estimations pour différentes tailles d'échantillons (de 2 à n)
``` {r}        
for (i in 2:n) {
  # Sous-échantillon de taille i
  x_sub <- x[1:i]
  y_sub <- y[1:i]
  # Estimations pour la taille d'échantillon i
  estim_a[i] <- a_estim(x_sub, y_sub)
  estim_b[i] <- b_estim(x_sub, y_sub)
}
``` 

On divise la fenêtre en 2 zones horizontales
``` {r}
par(mfrow=c(2,1)) 

plot(1:n, estim_a, type="l", col="blue",  ylim=c(a0-0.5, a0+0.5),
     xlab="Taille de l'échantillon", ylab="Estimation de a", main="Convergence de â vers a0")
abline(h=a0, col="red", lty=2)

plot(1:n, estim_b, type="l", col="green", ylim=c(b0-0.5, b0+0.5), 
     xlab="Taille de l'échantillon", ylab="Estimation de b", main="Convergence de ^b vers b0")
abline(h=b0, col="red", lty=2)
```

Pour remettre les paramètres graphiques par défaut
```{r}
par(mfrow=c(1,1))
```
## Q10
``` {r}
t <- 150
sigma2_estim <- function(){
  
  for(i in 1:t){
  x = round(runif(n,0,5), 2)
  y <- sapply(x, function(z){
      round(rnorm(1, a0+(b0*z), sqrt(s0) ))
    })
    
  estim_b = b_estim(x, y)
  estim_sig2 = sigma_estim(x, y)
  estim_a = mean(y) - estim_b*mean(x)
  pivot = (estim_a - a0) / sqrt((estim_sig2/n) * (1+(mean(x)^2) / (sum((x-mean(x))^2) / length(x)) ))
  
  }
  
  return(pivot)
}

simu = replicate(t, sigma2_estim ())
```
Tracés de l'Histogramme et courbe de densité d'une loi de Student à n-2 degrés de liberté
```{r}
hist(simu, freq = FALSE,col = "blue", xlab = "Valeurs de l'expression", main = "Histogramme et courbe de densité d'une loi de Student à n-2 degrés de liberté")
curve(dt(x, df = n - 2), add = TRUE,col="red", lwd = 2)

``` 

## Q11
``` {r}
sx2_calcul <- function(X) {
  return(sum((X - mean(X))^2) / length(X))
}
```

IC au niveau 1-alpha pour a
``` {r}
gen_IC_a <- function(X,Y,alpha) {
  sig2 = sigma_estim(X, Y)
  mean2 = mean(X)**2 
  sx2 = sx2_calcul(X)
  a = a_estim(X, Y)
  
  #Calcul du quantile de la loi Student
  t = qt(1 - alpha / 2, df = n - 2)
  
  #Formules pour borne inferieur et supérieure
  a_inf = a - t * sqrt((sig2/n) * (1+(mean2/sx2)))
  a_sup = a + t * sqrt((sig2/n) * (1+(mean2/sx2)))
  
  return(c(a_inf, a_sup))
}
```

IC au niveau 1-alpha pour b
``` {r}
gen_IC_b <- function(x,y,alpha) {
  sig2 = sigma_estim(x, y)
  sx2 = sx2_calcul(x)
  b = b_estim(x, y)
  
  #Calcul du quantile de la loi Student
  t = qt(1 - alpha / 2, df = n - 2)
  
  #Formules pour borne inferieur et supérieure
  b_inf = b - t * sqrt(sig2/(n*sx2))
  b_sup = b + t * sqrt(sig2/(n*sx2))
  
  return(c(b_inf, b_sup))
}
```

IC au niveau 1-alpha pour sigma2
```{r}
gen_IC_sigma <- function(x,y,alpha) {
  sig2 = sigma_estim(x, y)
  
  #Quantile Khi2 inférieur et supérieur
  fnInf = qchisq(1 - alpha / 2, df = n - 2) 
  fnSup = qchisq(alpha / 2, df = n - 2)
  
  #Formules pour borne inferieur et supérieure
  sig_inf = ((n-2)*sig2) / fnInf
  sig_sup = ((n-2)*sig2) / fnSup 
  
  return(c(sig_inf, sig_sup))
}

gen_IC_a(x,y,0.5)
gen_IC_b(x,y,0.5)
gen_IC_sigma(x,y,0.5)

``` 

## Q12
``` {r}
generate_ic <- function(gen_ic_function) {
  x <- runif(n, min = 0, max = 5)
  y <- rnorm(n, mean = a0 + b0 * x, sd = sqrt(s0))
  gen_ic_function(x, y, 0.5)
}

num_simulations <- 100
```

Répliquer la génération d'intervalles de confiance pour a, b et sigma^2
```{r}
ICs_a <- replicate(num_simulations, generate_ic(gen_IC_a))
ICs_b <- replicate(num_simulations, generate_ic(gen_IC_b))
ICs_s <- replicate(num_simulations, generate_ic(gen_IC_sigma))
```

## Q13
``` {r}
source("/Users/labuelgh/Documents/UTC/AC04/utils.R")    

plot_ICs(ICs_a, mean(ICs_a) ,main = "Intervalles de confiance a")
plot_ICs(ICs_b, mean(ICs_b) ,main = "Intervalles de confiance b")
plot_ICs(ICs_s, mean(ICs_s) ,main = "Intervalles de confiance sigma")
```
## III - Homoscédasticité, indépendance et normalité des résidus

## Q14

Mise en place de 4 graphiques sur une fenêtre
``` {r}
par(mfrow = c(2, 2))
``` 

On charge anscombe
``` {r}
data(anscombe)
``` 

Cette boucle passe à travers les quatre ensembles de données pour générer des graphiques de dispersion avec leurs lignes de régression respectives pour souligner la diversité des tendances malgré des statistiques similaires.
``` {r}
for(i in 1:4) {
  x <- anscombe[[paste0("x", i)]]
  y <- anscombe[[paste0("y", i)]]
  
  plot(x, y, main = sprintf("Ensemble %d", i), xlab = sprintf("x%d", i), ylab = sprintf("y%d", i),
       pch = 21, bg = 'lightblue', cex = 1.5)
  abline(lm(y ~ x), col = "red")  # ligne de régression en rouge
}
```

## Q15
``` {r}
results <- list()
for(i in 1:4) {
  x <- anscombe[[paste0("x", i)]]
  y <- anscombe[[paste0("y", i)]]
  
  # Effectuer la régression linéaire
  model <- lm(y ~ x)
  results[[i]] <- summary(model)
}
```

Affichage des résumés des régressions
``` {r}
results[[1]]
results[[2]]
results[[3]]
results[[4]]
```
Interprétation des résultats
  
Le coefficient de détermination R^2 nous permet de voir la proportion de variance expliquée par la régression, il doit en théorie nous permettre de vérifier si le modèle de régression linéaire est adapté aux jeux de données.  
Les quatre jeux de données présentent un coefficient de détermination très similaire, celui-ci avoisine les 0.67 en moyenne.   
Grâce à cela on peut estimer que la régression linéaire est adaptée aux quatre jeux de données.  
Hors, graphiquement on peut observer que les modèles 2 et 4 en particulier ne sont pas adaptés.  
Donc, on ne peut pas émettre de conclusion avec uniquement le coefficient de détermination.  

## Q16

Fonction pour générer les diagnostics de régression
``` {r}
diagnostic_plots <- function(model, dataset_name) {
  # Résidus
  residuals <- model$residuals
  std_residuals <- rstandard(model)
  fitted_values <- model$fitted.values
  x <- model$model[[2]]
  
  # Normalité des résidus
  par(mfrow = c(2, 2))
  
  # QQ plot
  qqnorm(residuals, main = paste("QQ Plot des résidus -", dataset_name))
  qqline(residuals, col = "red")
  
  # Histogramme des résidus corrigés
  hist(residuals, freq = FALSE, main = paste("Histogramme des résidus -", dataset_name))
  lines(density(residuals), col = "blue")
  curve(dnorm(x, mean = mean(residuals), sd = sd(residuals)), add = TRUE, col = "red")
  
  # Homoscédasticité
  plot(fitted_values, std_residuals, main = paste("Résidus standardisés vs. prédictions -", dataset_name),
       xlab = "Valeurs prédites", ylab = "Résidus standardisés")
  abline(h = 0, col = "red")
  
  # Indépendance
  plot(x, std_residuals, main = paste("Résidus standardisés vs. variable explicative -", dataset_name),
       xlab = "Valeurs de la variable explicative", ylab = "Résidus standardisés")
  abline(h = 0, col = "red")
  
  # Remettre les paramètres graphiques par défaut
  par(mfrow = c(1, 1))
}
```

Effectuer les diagnostics pour chaque modèle
``` {r}
for(i in 1:4) {
  x <- anscombe[[paste0("x", i)]]
  y <- anscombe[[paste0("y", i)]]
  
  # Effectuer la régression linéaire
  model <- lm(y ~ x)
  
  # Générer les diagnostics
  diagnostic_plots(model, paste(i))
}
```


Vérification empirique de la validité des hypothèses H

Les graphes sont ordonnés par jeu de données, il faut passer les onglets de plots pour visualiser les graphiques des différents jeux de données.  
  
**Homoscédasticité** : On ne doit pas observer de variation dans la dispertion.  
Les jeux de données 2,3 et 4 présentent de large variations dans la dispertion des résidus (hétéroscédaciticité).  
Cependant, les résidus du jeux de données 1 ne semblent pas varier selon x (homoscédasticité).  
  
**Normalité** : Loi normale centrée
Les quantiles du jeu de données 4 semble suivre les quantiles d'une loi normale centrée réduite N(0, sigma^2).
  
Cependant, ceux des jeux 2 et 3 ont des valeurs aberrantes nous ne pouvons donc pas émettre de conclusion pour ceux-ci.
  
Les quantiles du  jeux de données 1 sont trop éloignés des quantiles d'une loi normale.
  
## IV - Jeu de données réelles Real_Data
## Q17

Nous faisons le choix de nous baser sur le score de **happiness** pour l'étude de ce jeu de données. 

``` {r}
happiness <- read.csv("/Users/labuelgh/Documents/UTC/AC04/happiness_csv.csv")
summary(happiness)
```

Calcul des coefficients de corrélation entre les variables et le score de happiness
``` {r}
eco_h <- cor(happiness$Happiness.Score, happiness$Economy..GDP.per.Capita.)
print(eco_h)
sante_h <- cor(happiness$Happiness.Score, happiness$Health..Life.Expectancy.)
print(sante_h)
liberte_h <- cor(happiness$Happiness.Score, happiness$Freedom)
print(liberte_h)
famille_h <- cor(happiness$Happiness.Score, happiness$Family)
print(famille_h)
confiance_h <- cor(happiness$Happiness.Score, happiness$Trust..Government.Corruption.)
print(confiance_h)
generosite_h <- cor(happiness$Happiness.Score, happiness$Generosity)
print(generosite_h)
```
D'après les coefficients de corrélation, 3 des paramètres suivent la même tendance avec un coefficient élevé (Economy, Health et Family). 

Graphiques de dispersion 
```{r}
plot(happiness$Economy..GDP.per.Capita.,happiness$Happiness.Score, xlab = "Economy GDP per Capita", ylab = "Happiness Score")
plot(happiness$Health..Life.Expectancy.,happiness$Happiness.Score, xlab = "Health Life Expectancy", ylab = "Happiness Score")
plot(happiness$Freedom,happiness$Happiness.Score, xlab = "Freedom", ylab = "Happiness Score")
plot(happiness$Family,happiness$Happiness.Score, xlab = "Family", ylab = "Happiness Score")
plot(happiness$Trust..Government.Corruption.,happiness$Happiness.Score, xlab = "Trust Government Corruption", ylab = "Happiness Score")
plot(happiness$Generosity,happiness$Happiness.Score, xlab = "Generosity", ylab = "Happiness Score")

```
Les nuages de points confirment nos observations initiales. Economy est le paramètre avec un coefficient le plus élevé et le nuage de points qui est pertinent pour une régression linéaire.  


## Q18
``` {r}
Xeco <- happiness$Economy..GDP.per.Capita.
Yh <- happiness$Happiness.Score
plot(Xeco, Yh, main = "Droite des moindres carrés", col="blue")
estimah <- a_estim(Xeco, Yh)
print(estimah)
estimbh <- b_estim(Xeco, Yh)
print(estimbh)
abline(estimah, estimbh, col = "red")
lmh <- lm(Yh~Xeco)
abline(lmh, col = "green")
summary(lmh)
```
Nous avons estimé les paramètres grâce aux fonctions créées lors des questions précedentes et nous avons vérifié l'exactitude de nos estimations en les comparant à la fonction fournie par R.   
Nous remarquons que les droites des moindres carrés se confondent ce qui confirme la validité de nos fonctions.  

## Q19
``` {r}
confint(lmh)
```

Il y a 95% de chance que notre ordonnée à l'origine (intercept), estimah, se situe dans l'intervalle [3.236006; 3.761613] et que le coefficient de la pente (Xeco), estimbh, se situe dans l'intervalle [1.937667; 2.498787]

## Q20
``` {r}
coeff <- cor(Yh,Xeco)^2
```
Nous pouvons accéder à la valeur de R2 via la fonction cor. Nous avons une valeur de 0,6099 soit 60,99% ici. Or plus la valeur est proche de 1, meilleure est l'adaptation du modèle. Nous avons donc une bonne adéquation du modèle de régression linéaire aux données.

## Q21
``` {r}
verification <- cov(Xeco,Yh)^2/(sd(Xeco)^2*sd(Yh)^2)
```
Nous obtenons une valeur d'environ 0,6099 qui est identique à la valeur calculée à la question précédente.

## Q22
``` {r}
p_value <- summary(lmh)$coefficients["Xeco", "Pr(>|t|)"]
if (p_value < 0.05) {
  cat("Rejeter H0: Le coefficient de la pente est significativement différent de 0.\n")
} else {
  cat("Ne pas rejeter H0: Aucune preuve significative que le coefficient de la pente est différent de 0.\n")
}
```
p_value a pour valeur 1.050538e-33 ce qui nous indique que b est différent de 0. 

## Q23
``` {r}
# Régression linéaire
model <- lm(Yh ~ Xeco, data = happiness)

# Résidus
residuals <- model$residuals

# Diagramme quantile-quantile
qqnorm(residuals, main = "QQ Plot des résidus - Dataset Happiness")
qqline(residuals, col = "red")

```
Les résidus sont proportionnels aux résidus d'une loi normale centrée réduite.  

## Q24
``` {r}
# Graphe des résidus versus la variable explicative pour vérifier l'indépendance
plot(Xeco, residuals, main = "Résidus corrigés vs. Economy..GDP.per.Capita",
     xlab = "Economy..GDP.per.Capita", ylab = "Résidus corrigés",
     pch = 21, bg = 'lightblue', cex = 1.5)
abline(h = 0, col = "red")

par(mfrow = c(1, 1))
```
On peut voir que par rapport à l'abscisse (Economy..GDP.per.capita) du jeu de données, les résidus ne suivent pas de tendance discernable. On peut donc dire qu'ils sont indépendants.  
De plus, ils sont distribués de manière homogène autour de la ligne horizontale à zéro. Cela montre une homoscédasticité.  

## Q25

Calcul des intervalles de confiance et de prédiction 
``` {r}
Xprev <- data.frame(Xeco = c(1, 1.5))
intConfYh <- predict(lmh, Xprev, interval = "confidence", level = 0.95)
intPredYh <- predict(lmh, Xprev, interval = "prediction", level = 0.95)

```
Nous avons le tableau suivant pour les intervalles de confiance.
```{r}
print(intConfYh)
```

Nous avons le tableau suivant pour les intervalles de prédiction.
```{r}
print(intPredYh)
```


## Q26 - Conclusion
Au sein du jeu de données **Happiness**, une relation linéaire positive a été observée entre le score de bonheur et le PIB par habitant, expliquant une part significative de la variance. Les résidus du modèle respectent les conditions de normalité, d'homoscédasticité et d'indépendance, validant ainsi le modèle de régression linéaire.
